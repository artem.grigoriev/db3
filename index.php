<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();

    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        // Выводим сообщение пользователю.
        $messages[] = 'Спасибо, результаты сохранены.';
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    foreach (['name', 'email', 'birthday', 'contract'] as $key) {
        $errors[$key] = !empty($_COOKIE[$key . '_error']);
        if ($errors[$key] != '')
            $hasErrors = TRUE;
    }


    // TODO: аналогично все поля.

    // Выдаем сообщения об ошибках.
    if (!empty($errors['fio'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните имя.</div>';
    }

    if (!empty($errors['fio'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните имя.</div>';
    }

    if (!empty($errors['email'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните email.</div>';
    }

    if (!empty($errors['bd'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('bd_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните дату.</div>';
    }


    // Складываем предыдущие значения полей в массив, если есть.
    // При этом санитизуем все данные для безопасного отображения в браузере.
    $values = array();
    foreach (['fio', 'email', 'bd', 'sex', 'limbs', 'bio', 'accept'] as $key) {
        $values[$key] = !array_key_exists($key . '_value', $_COOKIE) ? $defaultValues[$key] : strip_tags($_COOKIE[$key . '_value']);
    }
    $values['abilities'] = array();
    $values['abilities']['0'] = empty($_COOKIE['superpowers_0_value']) ? '' : strip_tags($_COOKIE['superpowers_0_value']);
    $values['abilities']['1'] = empty($_COOKIE['superpowers_1_value']) ? '' : strip_tags($_COOKIE['superpowers_1_value']);
    $values['abilities']['2'] = empty($_COOKIE['superpowers_2_value']) ? '' : strip_tags($_COOKIE['superpowers_2_value']);




    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (empty($errors) && !empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {

        $user = 'u40077';
        $pass = '3053723';
        $db = new PDO('mysql:host=localhost;dbname=u40077', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        $stmt1 = $db->prepare('SELECT fio, email, bd, sex, limbs, bio FROM formtab5 WHERE form_id = ?');
        $stmt1->execute([$_SESSION['uid']]);
        $row = $stmt1->fetch(PDO::FETCH_ASSOC);
        $values['fio'] = strip_tags($row['fio']);
        $values['email'] = strip_tags($row['email']);
        $values['bd'] = strip_tags($row['bd']);
        $values['sex'] = strip_tags($row['sex']);
        $values['limbs'] = strip_tags($row['limbs']);
        $values['bio'] = strip_tags($row['bio']);

        $stmt2 = $db->prepare('SELECT ability_id FROM form_ability WHERE form_id = ?');
        $stmt2->execute([$_SESSION['uid']]);
        while($row = $stmt2->fetch(PDO::FETCH_ASSOC)) {
            $values['abilities'][$row['abid']] = TRUE;
        }


        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }

    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['bd'])) {
        setcookie('bd_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('bd_value', $_POST['bd'], time() + 30 * 24 * 60 * 60);
    }


    if (empty($_POST['abilities'])) {
        setcookie('ab_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('ab_value', $_POST['abilities'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['accept'])) {
        setcookie('ac_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('ac_value', $_POST['accept'], time() + 30 * 24 * 60 * 60);
    }


// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('bd_error', '', 100000);
        setcookie('ab_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('ac_error', '', 100000);


    }

    // Проверяем меняютсяgi ли ранее сохраненные данные или отправляются новые.
    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {

        $user = 'u40077';
        $pass = '3053723';
        $db = new PDO('mysql:host=localhost;dbname=u40077', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        $stmt1 = $db->prepare('UPDATE forms SET fio=?, email=?, bd=?, sex=?, limbs=?, bio=? WHERE form_id = ?');
        $stmt1->execute([$values['fio'], $values['email'], $values['bd'], $values['sex'], $values['limbs'], $values['bio'], $_SESSION['uid']]);

        $stmt2 = $db->prepare('DELETE FROM form_ab WHERE id = ?');
        $stmt2->execute([$_SESSION['uid']]);

        $stmt3 = $db->prepare("INSERT INTO form_ab SET id = ?, abid = ?");
        foreach ($_POST['abilities'] as $s){
            $stmt3 -> execute([$_SESSION['uid'], intval($s)]);
        }


        // TODO: перезаписать данные в БД новыми данными,
        // кроме логина и пароля.
    }
    else {
        // Генерируем уникальный логин и пароль.
        // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
        $id = uniqid();
        $hash = md5($id);
        $login = substr($hash, 0, 8);
        $pass = substr($hash, 12);
        $pass_hash = substr(hash("sha256", $pass), 0, 20);

        // Сохраняем в Cookies.
        setcookie('login', $login);
        setcookie('pass', $pass);

        // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
        // ...
        $user = 'u40077';
        $pass_db = '3053723';
        $db = new PDO('mysql:host=localhost;dbname=u40077', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));
        $stmt1 = $db->prepare("INSERT INTO form5 SET fio = ?, email = ?, bd = ?, 
      sex = ? , limbs = ?, bio = ?, login = ?, pass_hash = ?");
        $stmt1 -> execute([$_POST['fio'], $_POST['email'], $_POST['bd'],
            $_POST['sex'], $_POST['limbs'], $_POST['bio'], $login, $pass_hash]);
        $stmt2 = $db->prepare("INSERT INTO form_ab SET id = ?, abid = ?");
        $id = $db->lastInsertId();
        foreach ($_POST['abilities'] as $s)
            $stmt2 -> execute([$id, $s]);
    }



// Сохраняем куку с признаком успешного сохранения.
setcookie('save', '1');

// Делаем перенаправление.
header('Location: ./');
}

